﻿using MiTienditaBackCore.COM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace MiTienditaBackCore.DAT
{
    public class ProductData
    {
        private SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

        public ProductData()
        {
            builder.DataSource = "EDWARD-PORTATIL\\MPRO";
            builder.InitialCatalog = "MiTiendita";
            builder.IntegratedSecurity = true;
        }

        public bool CreateProduct(Products product)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = conn.CreateCommand())
                    {
                        comm.CommandText = @"INSERT INTO Products(
                                productName,
                                productValue
                                )
                            VALUES (
                                @productName,
                                @productValue
                                )";
                        comm.Parameters.AddWithValue("@productName", product.productName);
                        comm.Parameters.AddWithValue("@productValue", product.productValue);
                        comm.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool UpdateProduct(int idProduct, Products product)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = conn.CreateCommand())
                    {
                        comm.CommandText = @"UPDATE Products SET
                                productName = @productName,
                                productValue = @productValue
                            WHERE
                                productId = @productId";
                        comm.Parameters.AddWithValue("@productName", product.productName);
                        comm.Parameters.AddWithValue("@productValue", product.productValue);
                        comm.Parameters.AddWithValue("@productId", idProduct);
                        comm.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool DeleteProduct(int idProduct)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = conn.CreateCommand())
                    {
                        comm.CommandText = @"DELETE FROM Products 
                            WHERE productId = @productId";
                        comm.Parameters.AddWithValue("@productId", idProduct);
                        comm.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Products> GetProducts()
        {
            List<Products> products = new List<Products>();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "SELECT * FROM Products";

                    using (SqlCommand comm = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = comm.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                products.Add(new Products()
                                {
                                    productId = Convert.ToInt32(reader["productId"]),
                                    productName = reader["productName"].ToString(),
                                    productValue = Convert.ToDecimal(reader["productValue"])
                                });
                            }
                        }
                    }
                }
                return products;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Products GetProductById(int idProduct)
        {
            Products products = new Products();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    String sql = "SELECT * FROM Products WHERE productId = @productId";

                    using (SqlCommand comm = new SqlCommand(sql, connection))
                    {
                        comm.Parameters.Add(new SqlParameter("@productId", idProduct));
                        using (SqlDataReader reader = comm.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                products.productId = Convert.ToInt32(reader["productId"]);
                                products.productName = reader["productName"].ToString();
                                products.productValue = Convert.ToDecimal(reader["productValue"]);
                            }
                        }
                    }
                }
                return products;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Products GetProductByName(string productName)
        {
            Products products = new Products();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    String sql = "SELECT * FROM customers WHERE productName = @productName";

                    using (SqlCommand comm = new SqlCommand(sql, connection))
                    {
                        comm.Parameters.Add(new SqlParameter("@productName", productName));
                        using (SqlDataReader reader = comm.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                products.productId = Convert.ToInt32(reader["productId"]);
                                products.productName = reader["productName"].ToString();
                                products.productValue = Convert.ToDecimal(reader["productValue"]);
                            }
                        }
                    }
                }
                return products;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
