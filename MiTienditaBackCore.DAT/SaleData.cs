﻿using MiTienditaBackCore.COM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace MiTienditaBackCore.DAT
{
    public class SaleData
    {
        private SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

        public SaleData()
        {
            builder.DataSource = "EDWARD-PORTATIL\\MPRO";
            builder.InitialCatalog = "MiTiendita";
            builder.IntegratedSecurity = true;
        }

        public bool CreateSale(Sales sale)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = conn.CreateCommand())
                    {
                        comm.CommandText = @"INSERT INTO Sales(
                                productId,
                                customerId,
                                quantity
                                )
                            VALUES (
                                @productId,
                                @customerId,
                                @quantity
                                )";
                        comm.Parameters.AddWithValue("@productId", sale.productId);
                        comm.Parameters.AddWithValue("@customerId", sale.customerId);
                        comm.Parameters.AddWithValue("@quantity", sale.quantity);
                        comm.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool UpdateSale(int idProduct, Sales sales)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = conn.CreateCommand())
                    {
                        comm.CommandText = @"UPDATE Sales SET
                                quantity = @quantity
                            WHERE
                                saleId = @saleId";
                        comm.Parameters.AddWithValue("@quantity", sales.quantity);
                        comm.Parameters.AddWithValue("@saleId", sales.saleId);
                        comm.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool DeleteSale(int idSale)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = conn.CreateCommand())
                    {
                        comm.CommandText = @"DELETE FROM Sales 
                            WHERE saleId = @saleId";
                        comm.Parameters.AddWithValue("@saleId", idSale);
                        comm.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<SalesProduct> GetSales()
        {
            List<SalesProduct> sales = new List<SalesProduct>();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = @"SELECT
                                    s.saleId,
                                    s.productId,
                                    p.productName,
                                    p.productValue,
                                    s.quantity
                                    FROM Sales s 
                                    INNER JOIN Products p ON p.productId = s.productId";

                    using (SqlCommand comm = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = comm.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                sales.Add(new SalesProduct()
                                {
                                    saleId = Convert.ToInt32(reader["saleId"]),
                                    productName = reader["productName"].ToString(),
                                    productValue = Convert.ToDecimal(reader["productValue"]),
                                    quantity = Convert.ToInt32(reader["quantity"]),
                                });
                            }
                        }
                    }
                }
                return sales;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<SalesProduct> GetSaleByCustomerId(int customerId)
        {
            List<SalesProduct> sales = new List<SalesProduct>();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    String sql = @"SELECT
                                    s.saleId,
                                    s.productId,
                                    p.productName,
                                    p.productValue,
                                    s.quantity
                                    FROM Sales s 
                                    INNER JOIN Products p ON p.productId = s.productId
                                    WHERE customerId = @customerId";

                    using (SqlCommand comm = new SqlCommand(sql, connection))
                    {
                        comm.Parameters.Add(new SqlParameter("@customerId", customerId));
                        using (SqlDataReader reader = comm.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                sales.Add(new SalesProduct()
                                {
                                    saleId = Convert.ToInt32(reader["saleId"]),
                                    productName = reader["productName"].ToString(),
                                    productValue = Convert.ToDecimal(reader["productValue"]),
                                    quantity = Convert.ToInt32(reader["quantity"]),
                                });
                            }
                        }
                    }
                }
                return sales;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Sales GetSaleById(int idSale)
        {
            Sales sales = new Sales();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    String sql = "SELECT * FROM Sales WHERE saleId = @saleId";

                    using (SqlCommand comm = new SqlCommand(sql, connection))
                    {
                        comm.Parameters.Add(new SqlParameter("@saleId", idSale));
                        using (SqlDataReader reader = comm.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                sales.saleId = Convert.ToInt32(reader["saleId"]);
                                sales.productId = Convert.ToInt32(reader["productId"]);
                                sales.customerId = Convert.ToInt32(reader["customerId"]);
                                sales.quantity = Convert.ToInt32(reader["quantity"]);
                            }
                        }
                    }
                }
                return sales;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
