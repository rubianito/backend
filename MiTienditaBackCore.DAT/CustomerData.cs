﻿using MiTienditaBackCore.COM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace MiTienditaBackCore.DAT
{
    public class CustomerData
    {
        private SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

        public CustomerData()
        {
            builder.DataSource = "EDWARD-PORTATIL\\MPRO";
            builder.InitialCatalog = "MiTiendita";
            builder.IntegratedSecurity = true;
        }

        //public void prueba()
        //{
        //    try
        //    {
        //        SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

        //        builder.DataSource = "EDWARD-PORTATIL\\MPRO";
        //        //builder.UserID = "<your_username>";
        //        //builder.Password = "<your_password>";
        //        builder.InitialCatalog = "MiTiendita";
        //        builder.IntegratedSecurity = true;

        //        using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
        //        {
        //            Console.WriteLine("\nQuery data example:");
        //            Console.WriteLine("=========================================\n");

        //            connection.Open();

        //            String sql = "SELECT * FROM customers";

        //            using (SqlCommand command = new SqlCommand(sql, connection))
        //            {
        //                using (SqlDataReader reader = command.ExecuteReader())
        //                {
        //                    while (reader.Read())
        //                    {
        //                        Console.WriteLine("{0} {1}", reader["customerId"], reader.GetString(1));
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (SqlException e)
        //    {
        //        Console.WriteLine(e.ToString());
        //    }
        //    Console.WriteLine("\nDone. Press enter.");
        //    Console.ReadLine();
        //}



        public bool CreateCustomer(Customers customer)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = conn.CreateCommand())
                    {
                        comm.CommandText = @"INSERT INTO Customers(
                                customerName,
                                customerLastName,
                                documentNumber,
                                phoneNumber
                                )
                            VALUES (
                                @customerName,
                                @customerLastName,
                                @documentNumber,
                                @phoneNumber
                                )";
                        comm.Parameters.AddWithValue("@customerName", customer.customerName);
                        comm.Parameters.AddWithValue("@customerLastName", customer.customerLastName);
                        comm.Parameters.AddWithValue("@documentNumber", customer.documentNumber);
                        comm.Parameters.AddWithValue("@phoneNumber", customer.phoneNumber);
                        comm.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool UpdateCustomer(int idCustomer, Customers customer)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = conn.CreateCommand())
                    {
                        comm.CommandText = @"UPDATE Customers SET
                                customerName = @customerName,
                                customerLastName = @customerLastName,
                                documentNumber = @documentNumber,
                                phoneNumber = @phoneNumber
                            WHERE
                                customerId = @customerId";
                        comm.Parameters.AddWithValue("@customerName", customer.customerName);
                        comm.Parameters.AddWithValue("@customerLastName", customer.customerLastName);
                        comm.Parameters.AddWithValue("@documentNumber", customer.documentNumber);
                        comm.Parameters.AddWithValue("@phoneNumber", customer.phoneNumber);
                        comm.Parameters.AddWithValue("@customerId", customer.customerId);
                        comm.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool DeleteCustomer(int idCustomer)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = conn.CreateCommand())
                    {
                        comm.CommandText = @"DELETE FROM Customers 
                            WHERE customerId = @customerId";
                        comm.Parameters.AddWithValue("@customerId", idCustomer);
                        comm.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Customers> GetCustomers()
        {
            List<Customers> customers = new List<Customers>();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    String sql = "SELECT * FROM Customers";

                    using (SqlCommand comm = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = comm.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customers.Add(new Customers()
                                {
                                    customerId = Convert.ToInt32(reader["customerId"]),
                                    customerName = reader["customerName"].ToString(),
                                    customerLastName = reader["customerLastName"].ToString(),
                                    documentNumber = reader["documentNumber"].ToString(),
                                    phoneNumber = reader["phoneNumber"].ToString()
                                });
                            }
                        }
                    }
                }
                return customers;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Customers GetCustomerById(int idCustomer)
        {
            Customers customers = new Customers();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    String sql = "SELECT * FROM Customers WHERE customerId = @customerId";

                    using (SqlCommand comm = new SqlCommand(sql, connection))
                    {
                        comm.Parameters.Add(new SqlParameter("@customerId", idCustomer));
                        using (SqlDataReader reader = comm.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                customers.customerId = Convert.ToInt32(reader["customerId"]);
                                customers.customerName = reader["customerName"].ToString();
                                customers.customerLastName = reader["customerLastName"].ToString();
                                customers.documentNumber = reader["documentNumber"].ToString();
                                customers.phoneNumber = reader["phoneNumber"].ToString();
                            }
                        }
                    }
                }
                return customers;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Customers GetCustomerByDocument(string documentNumber)
        {
            Customers customers = new Customers();
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    String sql = "SELECT * FROM Customers WHERE documentNumber = @documentNumber";

                    using (SqlCommand comm = new SqlCommand(sql, connection))
                    {
                        comm.Parameters.Add(new SqlParameter("@documentNumber", documentNumber));
                        using (SqlDataReader reader = comm.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                customers.customerId = Convert.ToInt32(reader["customerId"]);
                                customers.customerName = reader["customerName"].ToString();
                                customers.customerLastName = reader["customerLastName"].ToString();
                                customers.documentNumber = reader["documentNumber"].ToString();
                                customers.phoneNumber = reader["phoneNumber"].ToString();
                            }
                        }
                    }
                }
                return customers;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
