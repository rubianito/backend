﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiTienditaBackCore.COM
{
    public class Products
    {
        public int productId { get; set; }
        public string productName { get; set; }
        public Nullable<decimal> productValue { get; set; }
    }
}
