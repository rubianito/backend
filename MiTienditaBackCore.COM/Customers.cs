﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiTienditaBackCore.COM
{
    public class Customers
    {
        public int customerId { get; set; }
        public string customerName { get; set; }
        public string customerLastName { get; set; }
        public string documentNumber { get; set; }
        public string phoneNumber { get; set; }
    }
}
