﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiTienditaBackCore.COM
{
    public class SalesProduct
    {
        public int saleId { get; set; }
        public string productName { get; set; }
        public Nullable<decimal> productValue { get; set; }
        public Nullable<int> quantity { get; set; }
    }
}
