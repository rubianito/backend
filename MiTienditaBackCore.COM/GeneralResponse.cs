﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiTienditaBackCore.COM
{
    public class GeneralResponse<T>
    {
        public bool Error { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }
}
