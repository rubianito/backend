﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiTienditaBackCore.COM
{
    public class Sales
    {
        public int saleId { get; set; }
        public Nullable<int> productId { get; set; }
        public Nullable<int> customerId { get; set; }
        public Nullable<int> quantity { get; set; }
    }
}
