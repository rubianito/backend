﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MiTienditaBackCore.BSS;
using MiTienditaBackCore.COM;

namespace MiTienditaBackCore.Controllers
{
    [Route("api/[Controller]")]
    public class SaleController : Controller
    {
        [HttpPost]
        [Route("CreateSale")]
        public ActionResult CreateSale([FromBody] Sales sale)
        {
            GeneralResponse<bool> generalResponse = new GeneralResponse<bool>();
            SaleBusiness SaleBusiness = new SaleBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = SaleBusiness.CreateSale(sale);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpPut]
        [Route("UpdateSale")]
        public ActionResult UpdateSale(int idSale, [FromBody] Sales sale)
        {
            GeneralResponse<bool> generalResponse = new GeneralResponse<bool>();
            SaleBusiness SaleBusiness = new SaleBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = SaleBusiness.UpdateSale(idSale, sale);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpPost]
        [Route("DeleteSale")]
        public ActionResult DeleteSale(int idSale)
        {
            GeneralResponse<bool> generalResponse = new GeneralResponse<bool>();
            SaleBusiness SaleBusiness = new SaleBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = SaleBusiness.DeleteSale(idSale);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpGet]
        [Route("GetSales")]
        public ActionResult GetSales()
        {
            GeneralResponse<List<SalesProduct>> generalResponse = new GeneralResponse<List<SalesProduct>>();
            SaleBusiness SaleBusiness = new SaleBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = SaleBusiness.GetSales();
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpGet]
        [Route("GetSaleById")]
        public ActionResult GetSaleById(int idSale)
        {
            GeneralResponse<Sales> generalResponse = new GeneralResponse<Sales>();
            SaleBusiness SaleBusiness = new SaleBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = SaleBusiness.GetSaleById(idSale);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpGet]
        [Route("GetSaleByCustomerId")]
        public ActionResult GetSaleByCustomerId(int customerId)
        {
            GeneralResponse<List<SalesProduct>> generalResponse = new GeneralResponse<List<SalesProduct>>();
            SaleBusiness SaleBusiness = new SaleBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = SaleBusiness.GetSaleByCustomerId(customerId);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }
    }
}