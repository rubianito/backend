﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MiTienditaBackCore.BSS;
using MiTienditaBackCore.COM;

namespace MiTienditaBackCore.Controllers
{
    [Route("api/[Controller]")]
    public class ProductController : Controller
    {
        [HttpPost]
        [Route("CreateProduct")]
        public ActionResult CreateProduct([FromBody] Products product)
        {
            GeneralResponse<bool> generalResponse = new GeneralResponse<bool>();
            ProductBusiness ProductBusiness = new ProductBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = ProductBusiness.CreateProduct(product);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpPut]
        [Route("UpdateProduct")]
        public ActionResult UpdateProduct(int idProduct, [FromBody] Products product)
        {
            GeneralResponse<bool> generalResponse = new GeneralResponse<bool>();
            ProductBusiness ProductBusiness = new ProductBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = ProductBusiness.UpdateProduct(idProduct, product);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpPost]
        [Route("DeleteProduct")]
        public ActionResult DeleteProduct(int idProduct)
        {
            GeneralResponse<bool> generalResponse = new GeneralResponse<bool>();
            ProductBusiness ProductBusiness = new ProductBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = ProductBusiness.DeleteProduct(idProduct);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpGet]
        [Route("GetProducts")]
        public ActionResult GetProducts()
        {
            GeneralResponse<List<Products>> generalResponse = new GeneralResponse<List<Products>>();
            ProductBusiness ProductBusiness = new ProductBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = ProductBusiness.GetProducts();
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpGet]
        [Route("GetProductById")]
        public ActionResult GetProductById(int idProduct)
        {
            GeneralResponse<Products> generalResponse = new GeneralResponse<Products>();
            ProductBusiness ProductBusiness = new ProductBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = ProductBusiness.GetProductById(idProduct);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpGet]
        [Route("GetProductByName")]
        public ActionResult GetProductByName(string productName)
        {
            GeneralResponse<Products> generalResponse = new GeneralResponse<Products>();
            ProductBusiness ProductBusiness = new ProductBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = ProductBusiness.GetProductByName(productName);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }
    }
}