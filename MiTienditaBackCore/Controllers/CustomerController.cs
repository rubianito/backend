﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MiTienditaBackCore.BSS;
using MiTienditaBackCore.COM;

namespace MiTienditaBackCore.Controllers
{
    [Route("api/[Controller]")]
    public class CustomerController : Controller
    {
        [HttpPost]
        [Route("CreateCustomer")]
        public ActionResult CreateCustomer([FromBody] Customers customer)
        {
            GeneralResponse<bool> generalResponse = new GeneralResponse<bool>();
            CustomerBusiness CustomerBusiness = new CustomerBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = CustomerBusiness.CreateCustomer(customer);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpPut]
        [Route("UpdateCustomer")]
        public ActionResult UpdateCustomer(int idCustomer, [FromBody] Customers customer)
        {
            GeneralResponse<bool> generalResponse = new GeneralResponse<bool>();
            CustomerBusiness CustomerBusiness = new CustomerBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = CustomerBusiness.UpdateCustomer(idCustomer, customer);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpPost]
        [Route("DeleteCustomer")]
        public ActionResult DeleteCustomer(int idCustomer)
        {
            GeneralResponse<bool> generalResponse = new GeneralResponse<bool>();
            CustomerBusiness CustomerBusiness = new CustomerBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = CustomerBusiness.DeleteCustomer(idCustomer);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpGet]
        [Route("GetCustomers")]
        public ActionResult GetCustomers()
        {
            GeneralResponse<List<Customers>> generalResponse = new GeneralResponse<List<Customers>>();
            CustomerBusiness CustomerBusiness = new CustomerBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = CustomerBusiness.GetCustomers();
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpGet]
        [Route("GetCustomerById")]
        public ActionResult GetCustomerById(int idCustomer)
        {
            GeneralResponse<Customers> generalResponse = new GeneralResponse<Customers>();
            CustomerBusiness CustomerBusiness = new CustomerBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = CustomerBusiness.GetCustomerById(idCustomer);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }

        [HttpGet]
        [Route("GetCustomerByDocument")]
        public ActionResult GetCustomerByDocument(string documentNumber)
        {
            GeneralResponse<Customers> generalResponse = new GeneralResponse<Customers>();
            CustomerBusiness CustomerBusiness = new CustomerBusiness();
            try
            {
                generalResponse.Error = false;
                generalResponse.Result = CustomerBusiness.GetCustomerByDocument(documentNumber);
                return Ok(generalResponse);
            }
            catch (Exception e)
            {
                generalResponse.Error = true;
                generalResponse.Message = e.Message;
                return BadRequest(generalResponse);
            }
        }
    }
}