﻿using MiTienditaBackCore.COM;
using MiTienditaBackCore.DAT;
using System;
using System.Collections.Generic;

namespace MiTienditaBackCore.BSS
{
    public class CustomerBusiness
    {
        public bool CreateCustomer(Customers customer)
        {
            try
            {
                CustomerData customerData = new CustomerData();
                return customerData.CreateCustomer(customer);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool UpdateCustomer(int idCustomer, Customers customer)
        {
            try
            {
                CustomerData customerData = new CustomerData();
                return customerData.UpdateCustomer(idCustomer, customer);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool DeleteCustomer(int idCustomer)
        {
            try
            {
                CustomerData customerData = new CustomerData();
                return customerData.DeleteCustomer(idCustomer);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Customers> GetCustomers()
        {
            try
            {
                CustomerData customerData = new CustomerData();
                return customerData.GetCustomers();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Customers GetCustomerById(int idCustomer)
        {
            try
            {
                CustomerData customerData = new CustomerData();
                return customerData.GetCustomerById(idCustomer);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Customers GetCustomerByDocument(string documentNumber)
        {
            try
            {
                CustomerData customerData = new CustomerData();
                return customerData.GetCustomerByDocument(documentNumber);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
