﻿using MiTienditaBackCore.COM;
using MiTienditaBackCore.DAT;
using System;
using System.Collections.Generic;
using System.Text;

namespace MiTienditaBackCore.BSS
{
    public class SaleBusiness
    {
        public bool CreateSale(Sales sale)
        {
            try
            {
                SaleData saleData = new SaleData();
                return saleData.CreateSale(sale);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool UpdateSale(int idSale, Sales sale)
        {
            try
            {
                SaleData saleData = new SaleData();
                return saleData.UpdateSale(idSale, sale);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool DeleteSale(int idSale)
        {
            try
            {
                SaleData saleData = new SaleData();
                return saleData.DeleteSale(idSale);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<SalesProduct> GetSales()
        {
            try
            {
                SaleData saleData = new SaleData();
                return saleData.GetSales();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Sales GetSaleById(int idSale)
        {
            try
            {
                SaleData saleData = new SaleData();
                return saleData.GetSaleById(idSale);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<SalesProduct> GetSaleByCustomerId(int customerId)
        {
            try
            {
                SaleData saleData = new SaleData();
                return saleData.GetSaleByCustomerId(customerId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
