﻿using MiTienditaBackCore.COM;
using MiTienditaBackCore.DAT;
using System;
using System.Collections.Generic;
using System.Text;

namespace MiTienditaBackCore.BSS
{
    public class ProductBusiness
    {
        public bool CreateProduct(Products product)
        {
            try
            {
                ProductData productData = new ProductData();
                return productData.CreateProduct(product);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool UpdateProduct(int idProduct, Products product)
        {
            try
            {
                ProductData productData = new ProductData();
                return productData.UpdateProduct(idProduct, product);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool DeleteProduct(int idProduct)
        {
            try
            {
                ProductData productData = new ProductData();
                return productData.DeleteProduct(idProduct);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Products> GetProducts()
        {
            try
            {
                ProductData productData = new ProductData();
                return productData.GetProducts();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Products GetProductById(int idProduct)
        {
            try
            {
                ProductData productData = new ProductData();
                return productData.GetProductById(idProduct);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Products GetProductByName(string productName)
        {
            try
            {
                ProductData productData = new ProductData();
                return productData.GetProductByName(productName);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
